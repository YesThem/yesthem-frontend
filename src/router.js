import Vue from 'vue'
import Router from 'vue-router'
import Register from './views/Register.vue'
import Preview from './views/Preview.vue'
import SolutionDetail from './views/SolutionDetail.vue'
import Login from './views/Login.vue'
import Edit from './views/Edit.vue'
import Store from '@/store'
import Home from './views/Home.vue'
import Help from './views/Help.vue'
import Indice from './views/Indice.vue'
import Admin from './views/Admin.vue'
import PasswordResetSendEmail from './views/PasswordResetSendEmail.vue'
import PasswordResetConfirm from './views/PasswordResetConfirm.vue'
import CompanyValidation from './views/CompanyValidation.vue'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    { path: '/home', name: 'home', component: Home },
    { path: '/register', name: 'register', component: Register },
    { path: '/login', name: 'login', component: Login },
    { path: '/login/:registered', name: 'login-registered', component: Login },
    { path: '/company/:identifier', component: Preview },
    { path: '/company/:identifier/edit', redirect: '/company/:identifier/edit/about' },
    { path: '/company/:identifier/edit/:editing', component: Edit },
    { path: '/company/:identifier/service/:solution', component: SolutionDetail },
    { path: '/company/:identifier/service/:solution/validate', component: CompanyValidation },
    { path: '/indice', name: 'indice', component: Indice },
    { path: '/help', name: 'help', component: Help },
    { path: '/admin', name: 'admin', component: Admin },
    { path: '/forgot-password', name: 'reset-password', component: PasswordResetSendEmail },
    { path: '/password-change/token=:token', name: 'PasswordResetConfirm', component: PasswordResetConfirm},
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    { path: '*', redirect: '/home' }
  ]
})

const companyLoad = async function (identifier) {
  await Store.dispatch('company/populate', identifier)
}

router.beforeEach((to, from, next) => {
  const isLoggedIn = Store.getters['auth/isLoggedIn']
  const currentUser = Store.getters['auth/currentUser']
  const isAdmin = Store.getters['auth/isAdmin']
  // const loggedUser = Store.getters['auth/currentUser'].username
  const path = to.path.toLowerCase()

  switch (path) {
    case '/home':
      next(isLoggedIn ? { path: '/company/' + currentUser.identifier } : true)
      break
    case '/register':
      next(isLoggedIn ? { path: '/company/' + currentUser.identifier } : true)
      break
    case '/login':
      next(isLoggedIn ? { path: '/company/' + currentUser.identifier } : true)
      break
    case '/forgot-password':
      next(isLoggedIn ? { path: '/company/' + currentUser.identifier } : true)
      break
    case '/password-change/token=:token':
      next(isLoggedIn ? { path: '/company/' + currentUser.identifier } : true)
      break
    case '/company/' + to.params.identifier:
      next(companyLoad(to.params.identifier))
      break
    case '/company/' + to.params.identifier + '/edit':
      next(currentUser.identifier === to.params.identifier ? true : { path: '/company/' + to.params.identifier })
      break
    case '/company/' + to.params.identifier + '/edit/' + to.params.editing:
      next(currentUser.identifier === to.params.identifier ? true : { path: '/company/' + to.params.identifier })
      break
    case '/company/' + to.params.identifier + '/service/' + to.params.solution:
      next(companyLoad(to.params.identifier))
      break
    case '/company/' + to.params.identifier + '/service/' + to.params.solution + '/validate':
      next(companyLoad(to.params.identifier))
      break
    // case '/company/' + to.params.identifier + '/edit/' + to.params.editing:
    //   next(currentUser.identifier === to.params.identifier ? companyLoad(to.params.identifier) : { path: '/company/' + to.params.identifier })
    //   break
    case '/admin':
      next(isAdmin ? true : { path: '/home' })
      break
    default:
      next()
  }
})

export default router
